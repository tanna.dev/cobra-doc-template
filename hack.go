package doc

import (
	"reflect"
	"unsafe"

	"github.com/spf13/cobra"
)

// via https://stackoverflow.com/a/60598827
func getUnexportedField(field reflect.Value) interface{} {
	return reflect.NewAt(field.Type(), unsafe.Pointer(field.UnsafeAddr())).Elem().Interface()
}

func getHelpCommand(c *cobra.Command) *cobra.Command {
	val := getUnexportedField(reflect.ValueOf(c).Elem().FieldByName("helpCommand"))
	if helpCommand, ok := val.(*cobra.Command); ok {
		return helpCommand
	}

	return nil
}
