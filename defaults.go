package doc

import "strings"

func CommandNameToMarkdownFilename(name string) string {
	filename := name + ".md"
	filename = strings.ReplaceAll(filename, " ", "_")
	return filename
}
