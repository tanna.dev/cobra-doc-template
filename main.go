package doc

import (
	"fmt"
	"sort"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

// CommandDoc contains all metadata about a single *cobra.Command's documentation
type CommandDoc struct {
	Command Command
	Now     string
}

// Command contains the single *cobra.Command's documentation and any relevant fields for documentation
type Command struct {
	Name       string
	Short      string
	Long       string
	Example    string
	Deprecated string
	Use        string

	NonInheritedFlags Flags
	InheritedFlags    Flags

	HasSeeAlso bool
	Parent     *ParentCommand
	Children   []ChildCommand
}

func (c Command) IsDeprecated() bool {
	return len(c.Deprecated) > 0
}

type ParentCommand struct {
	Name  string
	Short string
}

type ChildCommand struct {
	Name  string
	Short string
}

type Flags struct {
	Usage string
	Flags []Flag
}

type Flag struct {
	Name                string
	Shorthand           string
	ShorthandDeprecated string
	Deprecated          string
	// Type references the type of variable the **??** TODO
	Type  string
	Usage string

	// DefaultValue corresponds with pflag's DefValue: default value (as text); for usage message
	DefaultValue string
	// DefaultValueIfNoOptionProvided corresponds with pflag's NoOptDefVal: default value (as text); if the flag is on the command line without any options
	DefaultValueIfNoOptionProvided string
}

func (flag Flag) NameAndShorthand() string {
	if flag.Shorthand != "" && flag.ShorthandDeprecated == "" {
		return fmt.Sprintf("-%s, --%s", flag.Shorthand, flag.Name)
	} else {
		return fmt.Sprintf("--%s", flag.Name)
	}
}

func ParseCommand(cmd *cobra.Command) (CommandDoc, error) {
	if cmd == nil {
		return CommandDoc{}, fmt.Errorf("the provided cobra.Command was <nil>")
	}

	d := CommandDoc{}

	cmd.InitDefaultHelpCmd()
	cmd.InitDefaultHelpFlag()

	d.Command.Name = cmd.CommandPath()
	d.Command.Short = cmd.Short
	d.Command.Long = cmd.Long
	d.Command.Example = cmd.Example
	d.Command.Use = cmd.Use

	d.Command.NonInheritedFlags.Usage = cmd.NonInheritedFlags().FlagUsages()
	d.Command.NonInheritedFlags.Flags = parseFlags(cmd.NonInheritedFlags())
	d.Command.InheritedFlags.Usage = cmd.InheritedFlags().FlagUsages()
	d.Command.InheritedFlags.Flags = parseFlags(cmd.InheritedFlags())

	parseFlags(cmd.InheritedFlags())
	parseFlags(cmd.NonInheritedFlags())

	d.Command.HasSeeAlso = hasSeeAlso(cmd)
	d.Command.Deprecated = cmd.Deprecated

	if cmd.HasParent() {
		parent := cmd.Parent()
		pname := parent.CommandPath()
		d.Command.Parent = &ParentCommand{
			Name:  pname,
			Short: parent.Short,
		}
	}

	children := cmd.Commands()
	sort.Sort(byName(children))

	for _, child := range children {
		if !IsAvailableCommand(child) || child.IsAdditionalHelpTopicCommand() {
			continue
		}
		cname := d.Command.Name + " " + child.Name()
		d.Command.Children = append(d.Command.Children, ChildCommand{
			Name:  cname,
			Short: child.Short,
		})
	}

	d.Now = time.Now().Format("2-Jan-2006")
	return d, nil
}

// IsAvailableCommand determines if a command is available as a non-help command
// (this includes all non hidden commands).
// Slightly modified version of https://pkg.go.dev/github.com/spf13/cobra#Command.IsAvailableCommand to work around https://github.com/spf13/cobra/issues/2032, and make it so deprecated commands are rendered
func IsAvailableCommand(c *cobra.Command) bool {
	if c.Hidden {
		return false
	}

	if c.HasParent() && getHelpCommand(c.Parent()) == c {
		return false
	}

	if c.Runnable() || c.HasAvailableSubCommands() {
		return true
	}

	return false
}

func parseFlags(flags *pflag.FlagSet) []Flag {
	var allFlags []Flag

	flags.VisitAll(func(flag *pflag.Flag) {
		if flag.Hidden {
			return
		}

		ret := Flag{
			Name:                flag.Name,
			Shorthand:           flag.Shorthand,
			ShorthandDeprecated: flag.ShorthandDeprecated,
			Deprecated:          flag.Deprecated,
		}

		ret.Type, ret.Usage = pflag.UnquoteUsage(flag)

		if flag.NoOptDefVal != "" {
			switch flag.Value.Type() {
			case "string":
				ret.DefaultValueIfNoOptionProvided = fmt.Sprintf("[=\"%s\"]", flag.NoOptDefVal)
			case "bool":
				if flag.NoOptDefVal != "true" {
					ret.DefaultValueIfNoOptionProvided = fmt.Sprintf("[=%s]", flag.NoOptDefVal)
				}
			case "count":
				if flag.NoOptDefVal != "+1" {
					ret.DefaultValueIfNoOptionProvided = fmt.Sprintf("[=%s]", flag.NoOptDefVal)
				}
			default:
				ret.DefaultValueIfNoOptionProvided = fmt.Sprintf("[=%s]", flag.NoOptDefVal)
			}
		}

		// NOTE isFlagDefaultZeroValue is not implemented
		allFlags = append(allFlags, ret)
	})

	return allFlags
}
