# Cobra documentation, from `text/template`s

This is an experimental piece of code to provide an idea of how we could generate Cobra's documentation using custom `text/template`s, as per [this feature request](https://github.com/spf13/cobra/issues/2084).

Usage can be found in https://gist.github.com/jamietanna/020c15bbe5e6b258391fa3ded9861e38.
